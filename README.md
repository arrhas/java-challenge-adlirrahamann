# java challenge adlirrahaman



## Getting started

Berikut Penjelasan singkat :

di dalam git ini terdapat :
- code yang sudah dibuat
- unitest dalam code nya
- API DOCS ( Collection postman)
- database auto generate table dalam code menggunakan postgre

Step Menjalankan Code dan postman :
- pastikan code pada git sudah di clone untuk di jalankan pada IDE nya
- tunggu proses build selesai
- jalankan command berikut pada cmd yg ada di IDE : mvn clean install
- jalankan command untuk running program : mvn spring-boot:run
- buka Postman untuk melakukan pengetesan
- import collection yg ada pada gitlab ini
- jalankan sesuai dengan yang di inginkan , contoh : registrasi / login / edit / list