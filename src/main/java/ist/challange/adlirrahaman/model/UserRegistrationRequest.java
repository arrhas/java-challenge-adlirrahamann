package ist.challange.adlirrahaman.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class UserRegistrationRequest {

    private String username;
    private String password;
}
