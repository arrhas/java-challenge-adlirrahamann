package ist.challange.adlirrahaman.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class UserEditRequest {

    private String newUsername;
    private String newPassword;
}
