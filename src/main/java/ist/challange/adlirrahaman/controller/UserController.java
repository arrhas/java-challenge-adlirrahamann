package ist.challange.adlirrahaman.controller;

import ist.challange.adlirrahaman.entity.User;
import ist.challange.adlirrahaman.model.UserEditRequest;
import ist.challange.adlirrahaman.model.UserLoginRequest;
import ist.challange.adlirrahaman.model.UserRegistrationRequest;
import ist.challange.adlirrahaman.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestBody UserRegistrationRequest registrationRequest) {
        User registeredUser = userService.registerUser(registrationRequest.getUsername(), registrationRequest.getPassword());
        return new ResponseEntity<>(registeredUser, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserLoginRequest loginRequest) {
        ResponseEntity<String> response = userService.loginUser(loginRequest.getUsername(), loginRequest.getPassword());
        return response;
    }

    @GetMapping("/list")
    public ResponseEntity<List<User>> listUsers() {
        List<User> users = userService.listUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PutMapping("/edit/{userId}")
    public ResponseEntity<String> editUser(@PathVariable Integer userId, @RequestBody UserEditRequest editRequest) {
        ResponseEntity<String> response = userService.editUser(userId, editRequest.getNewUsername(), editRequest.getNewPassword());
        return response;
    }
}
