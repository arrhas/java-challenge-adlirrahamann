package ist.challange.adlirrahaman.service;

import ist.challange.adlirrahaman.entity.User;
import ist.challange.adlirrahaman.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User registerUser(String username, String password) {
        // Validasi apakah username sudah ada di database
        User existingUser = userRepository.findFirstByUsername(username);
        if (existingUser != null) {
            throw new UserAlreadyExistsException("Username sudah terpakai");
        }

        // Buat objek User baru
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);

        // Simpan user ke database
        return userRepository.save(newUser);
    }

    public ResponseEntity<String> loginUser(String username, String password) {
        // Validasi username dan password
        User user = userRepository.findFirstByUsername(username);
        if (user == null || !user.getPassword().equals(password)) {
            return new ResponseEntity<>("Username dan/atau password salah", HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity<>("Sukses Login", HttpStatus.OK);
    }

    public List<User> listUsers() {
        return userRepository.findAll();
    }

    public ResponseEntity<String> editUser(Integer userId, String newUsername, String newPassword) {
        // Cari user berdasarkan ID
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            return new ResponseEntity<>("User tidak ditemukan", HttpStatus.NOT_FOUND);
        }

        // Validasi apakah username sudah ada di database
        if (!user.getUsername().equals(newUsername) && userRepository.findFirstByUsername(newUsername) != null) {
            return new ResponseEntity<>("Username sudah terpakai", HttpStatus.CONFLICT);
        }

        // Validasi apakah password sama dengan password sebelumnya
        if (user.getPassword().equals(newPassword)) {
            return new ResponseEntity<>("Password tidak boleh sama dengan password sebelumnya", HttpStatus.BAD_REQUEST);
        }

        // Lakukan perubahan data user
        user.setUsername(newUsername);
        user.setPassword(newPassword);

        // Simpan perubahan ke database
        userRepository.save(user);
        System.out.println("user"+newUsername);
        System.out.println("password"+newPassword);
        return new ResponseEntity<>("Sukses mengedit data user", HttpStatus.CREATED);
    }

    public class UserAlreadyExistsException extends RuntimeException {
        public UserAlreadyExistsException(String message) {
            super(message);
        }
    }

}
