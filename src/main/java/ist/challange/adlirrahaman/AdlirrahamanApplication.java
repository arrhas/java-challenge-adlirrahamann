package ist.challange.adlirrahaman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdlirrahamanApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdlirrahamanApplication.class, args);
	}

}
