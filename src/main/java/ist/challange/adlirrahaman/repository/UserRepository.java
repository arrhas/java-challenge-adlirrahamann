package ist.challange.adlirrahaman.repository;

import ist.challange.adlirrahaman.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {
    User findFirstByUsername(String username);
}