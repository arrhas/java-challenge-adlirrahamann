package ist.challange.adlirrahaman;

import ist.challange.adlirrahaman.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class AdlirrahamanApplicationTests {


	@Autowired
	UserService userService;
	@Test
	void contextLoads() {
	}

	@Test
	public void testLogin_Success() {
		String username = "Shaka";
		String password = "password1";

		ResponseEntity<String> result = userService.loginUser(username, password);


		Assertions.assertEquals(200, result.getStatusCodeValue());
		Assertions.assertEquals("Sukses Login", result.getBody());
	}





}
